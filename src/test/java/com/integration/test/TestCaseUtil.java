package com.integration.test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class TestCaseUtil {
		
	@SuppressWarnings("unchecked")
	private static <T> T getResultObject(MvcResult result, Class<? extends Object> clazz) throws JsonProcessingException, UnsupportedEncodingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonResponse = mapper.readTree(result.getResponse().getContentAsString());
		JsonNode jsonResult = jsonResponse.findValue("data");
		T t = (T) mapper.readValue(jsonResult.toString(), clazz);
		return t;
	}
	
	private static <T> List<T> getResultList(MvcResult result, Class<? extends T[]> primitiveArrayClass) throws JsonProcessingException, UnsupportedEncodingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonResponse = mapper.readTree(result.getResponse().getContentAsString());
		JsonNode jsonResultList = jsonResponse.findValue("content");

		T[] resultArray = mapper.readValue(jsonResultList.toString(), primitiveArrayClass);
		List<T> resultList = new ArrayList<>(Arrays.asList(resultArray));
		return resultList;
	}
	
	public static <T> T insert(MockMvc mockMvc, String url, T pojo, Class<? extends Object> clazz) throws IOException, Exception{
		MvcResult result =
			mockMvc.perform(
	    			post(url)
	    			.content(JsonUtil.convertObjectToJsonString_Gson(pojo))
	    			.contentType(JsonUtil.APPLICATION_JSON)
				)
	    		.andExpect(status().isOk())
	    		.andExpect(content().contentType(JsonUtil.APPLICATION_HAL_JSON))
	    		.andReturn();
		
		return getResultObject(result, clazz);
	}
	
	public static <T> void insertFailure(MockMvc mockMvc, String url, T pojo) throws IOException, Exception{
		mockMvc.perform(
    			post(url)
    			.content(JsonUtil.convertObjectToJsonString_Gson(pojo))
    			.contentType(JsonUtil.APPLICATION_JSON)
			)
			.andDo(print())
			.andExpect(status().isBadRequest())
    		.andReturn();
	}
	
	public static <T> T update(MockMvc mockMvc, String url, T pojo, Class<? extends Object> clazz) throws IOException, Exception{
		MvcResult result =
			mockMvc.perform(
	    			put(url)
	    			.content(JsonUtil.convertObjectToJsonString_Gson(pojo))
	    			.contentType(JsonUtil.APPLICATION_JSON)
				)
	    	   	.andExpect(status().isOk())
	    	   	.andExpect(content().contentType(JsonUtil.APPLICATION_HAL_JSON))
	    	   	.andReturn();
		
		return getResultObject(result, clazz);
	}
	
	public static <T> void updateFailure(MockMvc mockMvc, String url, T pojo) throws IOException, Exception{
		mockMvc.perform(
    			put(url)
    			.content(JsonUtil.convertObjectToJsonString_Gson(pojo))
    			.contentType(JsonUtil.APPLICATION_JSON)
			)
    		.andDo(print())
    		.andExpect(status().isBadRequest())
    		.andReturn();
	}
	
	public static <T> T patchUpdate(MockMvc mockMvc, String url, T pojo, Class<? extends Object> clazz) throws IOException, Exception{
		MvcResult result =
			mockMvc.perform(
	    			patch(url)
	    			.content(JsonUtil.convertObjectToJsonString_Gson(pojo))
	    			.contentType(JsonUtil.APPLICATION_JSON)
				)
	    	   	.andExpect(status().isOk())
	    	   	.andExpect(content().contentType(JsonUtil.APPLICATION_HAL_JSON))
	    	   	.andReturn();
		
		return getResultObject(result, clazz);
	}
	
	public static <T> void patchUpdateFailure(MockMvc mockMvc, String url, T pojo) throws IOException, Exception{
		mockMvc.perform(
    			patch(url)
    			.content(JsonUtil.convertObjectToJsonString_Gson(pojo))
    			.contentType(JsonUtil.APPLICATION_JSON)
			)
    		.andDo(print())
    		.andExpect(status().isBadRequest())
    		.andReturn();
	}
	
	public static void deleteItem(MockMvc mockMvc, String url, Long id) throws Exception{
		mockMvc.perform(
				delete(url+id)
			)
			.andExpect(status().isOk())
		    .andReturn();
	}
	
	public static void deleteItemFailure(MockMvc mockMvc, String url, Long id) throws IOException, Exception{
		mockMvc.perform(
    			delete(url+id)
			)
			.andDo(print())
    		.andExpect(status().isBadRequest())
    		.andReturn();
	}
	
	public static <T> T getById(MockMvc mockMvc, String url, Long id, Class<? extends Object> clazz) throws Exception{
		MvcResult result = mockMvc.perform(get(url+id))
				.andExpect(status().isOk())
		        .andExpect(content().contentType(JsonUtil.APPLICATION_HAL_JSON))
		        .andReturn();
		
		return getResultObject(result, clazz);		
	}
	
	public static <T> List<T> getList(MockMvc mockMvc, String url, Class<? extends T[]> primitiveArrayClass) throws Exception{
		
		MvcResult result = 
				mockMvc.perform(get(url))
				.andExpect(status().isOk())
		        .andExpect(content().contentType(JsonUtil.APPLICATION_HAL_JSON))
		        .andReturn();
		
		return getResultList(result, primitiveArrayClass);
	}
	
	public static <T> void genericAction(MockMvc mockMvc, String url, List<T> pojoList, boolean success) throws Exception{
		
		mockMvc.perform(
    			put(url)
    			.content(JsonUtil.convertObjectToJsonString_Gson(pojoList))
    			.contentType(JsonUtil.APPLICATION_JSON)
			)
    		.andExpect(status().isOk())
    		.andExpect(content().contentType(JsonUtil.APPLICATION_HAL_JSON))
    		.andReturn();
	}
}
