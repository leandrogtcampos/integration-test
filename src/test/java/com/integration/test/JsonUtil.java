package com.integration.test;

import java.io.IOException;

import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.gson.GsonBuilder;

public final class JsonUtil {

	public static final MediaType APPLICATION_HAL_JSON = MediaTypes.HAL_JSON;
	public static final MediaType APPLICATION_JSON = MediaType.APPLICATION_JSON;
	 
	public static byte[] convertObjectToJsonBytes_Jackson(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }
    
    public static String convertObjectToJsonString_Gson(Object object){
    	return new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").create().toJson(object);
    }
    
    public static String convertObjectToJsonString_Jackson(Object object) throws JsonProcessingException{
	    ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
	    ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
	    String requestJson=ow.writeValueAsString(object);
	    return requestJson;
    }
}
