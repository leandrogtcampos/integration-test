package com.integration.test.store;

import java.util.function.Consumer;
import java.util.function.Supplier;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import com.integration.test.ControllerBaseTest;
import com.integrationtest.store.RequestCreateStore;
import com.integrationtest.store.RequestPatchStore;
import com.integrationtest.store.RequestUpdateStore;
import com.integrationtest.store.StoreResource;

@SuppressWarnings("unchecked")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StoreControllerTest <PostRequest extends RequestCreateStore, 
								  PutRequest extends RequestUpdateStore,
								  PatchRequest extends RequestPatchStore,
								  Response extends StoreResource> extends ControllerBaseTest<PostRequest, PutRequest, PatchRequest, Response>{

	@Before
    public void setUp() throws Exception {
    	super.setUp();
    	CONTROLLER_PATH += "/store";
    	clazz = StoreResource.class;
    	primitiveArrayClazz = StoreResource[].class;
    }
	
	@Override
	protected PostRequest getNew() {
		return (PostRequest) new RequestCreateStore("Store " + (int) Math.random(), "Rua 1, bairro Minas Brasil, Belo Horizonte - MG");
	}

	@Override
	protected void update() throws Exception {
	   	Response response = this.getPersisted();
	   	PutRequest req = (PutRequest) new RequestUpdateStore();
	   	
	   	req.setId(response.getId());
    	req.setName("Name Test");
    	req.setAddress("Adress Test");
    	
    	this.update(req);
	}
	
	@Override
	protected void patchUpdate() throws Exception {
		Response resp = this.getPersisted();
		PatchRequest req = (PatchRequest) new RequestPatchStore(resp.getId(), resp.getAddress() + "updated");
		
		this.patchUpdate(req);
	}

	@Override
	protected void delete() throws Exception {
		Response response = this.insert(getNew());
		this.delete(response.getId());
	}
	
	@Override
	protected void insertFailure(Supplier<PostRequest> supplier, Consumer<PostRequest> consumer) throws Exception {
		PostRequest req = supplier.get();
    	
    	//Name
    	req.setName(null);
    	consumer.accept(req);
    	
    	req.setName("");
    	consumer.accept(req);
    	
    	//Address
    	req.setAddress(null);
    	consumer.accept(req);
		
    	req.setAddress("");
    	consumer.accept(req);
	}

	@Override
	protected void updateFailure(Supplier<Response> supplier, Consumer<PutRequest> consumer) throws Exception {
		Response resp = supplier.get();
		PutRequest req = (PutRequest) new RequestUpdateStore(resp.getId(), resp.getName(), resp.getAddress());
		
		//Name
    	req.setName(null);
    	consumer.accept(req);
    	
    	req.setName("");
    	consumer.accept(req);
    	
    	//Address
    	req.setAddress(null);
    	consumer.accept(req);
		
    	req.setAddress("");
    	consumer.accept(req);
	}

	@Override
	protected void patchUpdateFailure(Supplier<Response> supplier, Consumer<PatchRequest> consumer) throws Exception {
		Response resp = supplier.get();
		PatchRequest req = (PatchRequest) new RequestPatchStore(resp.getId(), null);
		
		consumer.accept(req);
		
		req.setAddress("");
		consumer.accept(req);
	}
}