package com.integration.test;

import java.io.IOException;
import java.util.List;
import java.util.TimeZone;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.integrationtest.IntegrationTestApplication;

@SuppressWarnings("unchecked")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = IntegrationTestApplication.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public abstract class ControllerBaseTest<PostRequest, PutRequest, PatchRequest, Response> {
	
	protected String CONTROLLER_PATH="";
	protected String SEARCH_PARAMS = "?page=0&size=5";
	
	private MockMvc mockMvc;
	private @Autowired WebApplicationContext wac;

	protected Class<?> clazz;
	protected Class<? extends Object[]> primitiveArrayClazz;
	
	protected abstract PostRequest getNew();
	protected abstract void insertFailure(Supplier<PostRequest> supplier, Consumer<PostRequest> consumer)throws Exception;
	protected abstract void update()throws Exception;
	protected abstract void updateFailure(Supplier<Response> supplier, Consumer<PutRequest> consumer)throws Exception;
	protected abstract void patchUpdate()throws Exception;
	protected abstract void patchUpdateFailure(Supplier<Response> supplier, Consumer<PatchRequest> consumer)throws Exception;
	protected abstract void delete() throws Exception;
	
    public void setUp() throws Exception {
    	TimeZone.setDefault(TimeZone.getTimeZone("America/Sao_Paulo"));
    	mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();        
    }
    
	protected Response insert(PostRequest pojo){
		try{
			return (Response) TestCaseUtil.<PostRequest>insert(mockMvc, CONTROLLER_PATH, pojo, clazz);
		}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected Response update(PutRequest pojo){
    	try{
    		return (Response) TestCaseUtil.<PutRequest>update(mockMvc, CONTROLLER_PATH, pojo, clazz);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected Response patchUpdate(PatchRequest pojo) {
    	try{
    		return (Response) TestCaseUtil.<PatchRequest>patchUpdate(mockMvc, CONTROLLER_PATH, pojo, clazz);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected void insertFailure(PostRequest pojo){
    	try {
			TestCaseUtil.<PostRequest>insertFailure(mockMvc, CONTROLLER_PATH, pojo);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected void updateFailure(PutRequest pojo){
    	try{	
    		TestCaseUtil.<PutRequest>updateFailure(mockMvc, CONTROLLER_PATH, pojo);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected void patchUpdateFailure(PatchRequest pojo){
    	try{	
    		TestCaseUtil.<PatchRequest>patchUpdateFailure(mockMvc, CONTROLLER_PATH, pojo);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected void delete(Long id){
    	try{
    		TestCaseUtil.deleteItem(mockMvc, CONTROLLER_PATH + "/", id);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected void deleteFailure(Long id){
    	try{
    		TestCaseUtil.deleteItemFailure(mockMvc, CONTROLLER_PATH + "/", id);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected Response getById(Long id){
    	try{
    		return (Response) TestCaseUtil.<PostRequest>getById(mockMvc, CONTROLLER_PATH, id, clazz);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected List<Response> getList(){
    	try{
    		return (List<Response> ) TestCaseUtil.getList(mockMvc, CONTROLLER_PATH + SEARCH_PARAMS, primitiveArrayClazz);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
    protected Response getPersisted(){
    	try{
    		return this.getList().get(0);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
        
    protected void genericAction(String url, List<PostRequest> pojoList, boolean success){
    	try{
    		TestCaseUtil.<PostRequest>genericAction(mockMvc, url, pojoList, success);
    	}catch (Exception e){throw new RuntimeException(e);}
    }
    
	@Test
    public void t1_InsertTest()throws Exception{
    	this.insert(this.getNew()); 	
    }
    
    @Test
    public void t2_InsertFailureTest()throws Exception{
    	this.insertFailure(this::getNew, this::insertFailure);
    }
    
    @Test
    public void t3_UpdateTest()throws Exception{
    	this.update();
    }
    
    @Test
    public void t4_UpdateFailureTest()throws Exception{
    	this.updateFailure(this::getPersisted, this::updateFailure);
    }

    @Test
    public void t5_PatchUpdateTest()throws Exception{
    	this.patchUpdate();
    }
    
    @Test
    public void t6_PatchUpdateFailureTest()throws Exception{    	
    	this.patchUpdateFailure(this::getPersisted, this::patchUpdateFailure);
    }
    
    @Test
    public void t7_DeleteTest() throws IOException, Exception{
    	this.delete();
    }
    
    @Test
    public void t8_DeleteFailureTest() throws IOException, Exception{
    	this.deleteFailure(-1L);  	
    }
}
