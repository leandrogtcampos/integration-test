package com.integrationtest.dto;

public class Response<T> {

	private T data;
	
	public Response(T t) {
		this.data = t;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}
