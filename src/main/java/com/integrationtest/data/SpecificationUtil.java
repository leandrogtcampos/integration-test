package com.integrationtest.data;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.FetchParent;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

public class SpecificationUtil<T> {

	private Specification<T> specs;	
	
	public SpecificationUtil(){
		specs = Specification.where(
				new Specification<T>() {
					private static final long serialVersionUID = 1L;
					
					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						return cb.greaterThan(root.get("id"),-1);
					}
				});
	}
	
	public void and(Object... args){
		for(int i = 0; i < args.length; i++)
				andAux((String) args[i], args[++i]);
	}
	
	private void andAux(String paramName, Object paramValue){
		if(paramValue != null){
			specs = specs.and(new Specification<T>(){
					private static final long serialVersionUID = 1L;

					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						if(paramValue instanceof String)
							return cb.like(cb.lower(root.get(paramName)), "%" + ((String) paramValue).toLowerCase() + "%");
						else
							return cb.equal(root.get(paramName),paramValue);
					}
				}
			);
		}
	}
	
	public void andNotEqual(Object... args){
		for(int i = 0; i < args.length; i++)
			andNotEqualAux((String) args[i], args[++i]);
	}
	
	private void andNotEqualAux(String paramName, Object paramValue){
		if(paramValue != null){
			specs = specs.and(new Specification<T>(){
					private static final long serialVersionUID = 1L;
					
					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						return cb.notEqual(root.get(paramName),paramValue);
					}
				}
			);
		}
	}
	
	public void between(String paramName, Date first, Date second){
		if(first != null && second != null){
			specs = specs.and( new Specification<T>() {
					private static final long serialVersionUID = 1L;
					
					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						return cb.between(root.get((String)paramName),first,second);
					}
				}
			);
		}
	}
	
	public <U> void in(final List<U> list, final String listName){
		specs = specs.and( new Specification<T>() {
				private static final long serialVersionUID = 1L;
			
				public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					ListJoin<T, U> join = root.<T,U>joinList(listName);
					return join.in(list);
				}
			}
		);
	}
	
	public void isNotNull(String... args){
		for(String s : args){
			specs = specs.and(new Specification<T>(){
				private static final long serialVersionUID = 1L;
				
				public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					return cb.isNotNull(root.get(s));
				}
			});
		}
	}
	
	public void greaterThan(String paramName, String paramValue){
		if(paramValue != null){
			specs = specs.and(new Specification<T>(){
					private static final long serialVersionUID = 1L;

					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						return cb.greaterThan(root.get(paramName),paramValue);
					}
				}
			);
		}
	}
	
	/**
	 * It makes a fetch join on a lazy list of the searched object.
	 */
	public <U> void joinList(final String listName){
		specs = specs.and( new Specification<T>() {
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					FetchParent<T, U> fJoin = root.<T,U>fetch(listName, JoinType.INNER);
					ListJoin<T, U> joinList = (ListJoin<T, U>) fJoin;
					return joinList.getOn();
				}
			}
		);
	}
	
	public <U> void joinObject(final String objName){
		specs = specs.and( new Specification<T>() {
				private static final long serialVersionUID = 1L;

				public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
					FetchParent<T, U> fJoin = root.<T,U>fetch(objName, JoinType.INNER);
					Join<T, U> joinObj = (Join<T, U>) fJoin;
					return joinObj.getOn();
				}
			}
		);
	}
	
	public Specification<T> getSpec(){
		return specs;
	}
	
	public static <T> Specification<T> andStatic(Object... args){
		List<Object> params = new ArrayList<>(Arrays.asList(args));
		
		final String key = (String) params.remove(0);
		final Object val = params.remove(0);
		
		Specification<T> specs = null;
		
		if(val != null){
		specs = Specification.where(
				new Specification<T>() {
					private static final long serialVersionUID = 1L;
					
					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						if(val instanceof String)
							return cb.like(cb.lower(root.get(key)), ((String) val).toLowerCase());
						else
							return cb.equal(root.get(key),val);
					}
				});
		}
		
		List<Object> syncParams = Collections.synchronizedList(params);

		while(!syncParams.isEmpty()){
			final String keyP = (String) syncParams.remove(0);
			final Object valP = syncParams.remove(0);
			if(valP != null){
				specs = specs.and(new Specification<T>(){
						private static final long serialVersionUID = 1L;
					
						public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
							if(valP instanceof String)
								return cb.like(cb.lower(root.get(keyP)), ((String) valP).toLowerCase());
							else
								return cb.equal(root.get(keyP),valP);
						}
					}
				);
			}
		}
		
		return specs;
	}
	
	public static <T> Specification<T> likeStatic(Object... args){
		List<Object> params = new ArrayList<>(Arrays.asList(args));
		
		final String key = (String) (params.get(0) != null ? params.remove(0) : "");
		final Object val = params.remove(0);
		
		Specification<T> specs = null;
		
		if(val != null){
		specs = Specification.where(
				new Specification<T>() {
					private static final long serialVersionUID = 1L;
					
					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						if(val instanceof String)
							return cb.like(cb.lower(root.get(key)), "%" + ((String) val).toLowerCase() + "%");
						else
							return cb.equal(root.get(key),val);
					}
				});
		}
		
		List<Object> syncParams = Collections.synchronizedList(params);

		while(!syncParams.isEmpty()){
			final String keyP = (String) syncParams.remove(0);
			final Object valP = syncParams.remove(0);
			if(valP != null){
				specs = specs.and(new Specification<T>(){
						private static final long serialVersionUID = 1L;
						
						public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
							if(valP instanceof String)
								return cb.like(cb.lower(root.get(keyP)), "%" + ((String) valP).toLowerCase() + "%");
							else
								return cb.equal(root.get(keyP),valP);
						}
					}
				);
			}
		}
		
		return specs;
	}
	
	public static <T> Specification<T> likeNullAllowedStatic(Object... args){
		List<Object> params = new ArrayList<>(Arrays.asList(args));
		
		final String key = (String) params.remove(0);
		final Object val = params.remove(0);
		
		Specification<T> specs = null;
		
		specs = Specification.where(
				new Specification<T>() {
					private static final long serialVersionUID = 1L;
					
					public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
						if(val == null)
							return cb.isNull(root.get(key));
						else if(val instanceof String)
							return cb.like(cb.lower(root.get(key)), "%" + ((String) val).toLowerCase() + "%");
						else
							return cb.equal(root.get(key),val);
					}
				});
		
		List<Object> syncParams = Collections.synchronizedList(params);

		while(!syncParams.isEmpty()){
			final String keyP = (String) syncParams.remove(0);
			final Object valP = syncParams.remove(0);
				specs = specs.and(new Specification<T>(){
						private static final long serialVersionUID = 1L;
						
						public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
							if(valP == null)
								return cb.isNull(root.get(keyP));
							else if(valP instanceof String)
								return cb.like(cb.lower(root.get(keyP)), "%" + ((String) valP).toLowerCase() + "%");
							else
								return cb.equal(root.get(keyP),valP);
						}
					}
				);
		}
		
		return specs;
	}
}