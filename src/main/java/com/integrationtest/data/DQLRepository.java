package com.integrationtest.data;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface DQLRepository <Pojo extends Object, ID extends Object> extends PagingAndSortingRepository<Pojo, ID>, JpaSpecificationExecutor<Pojo>{}
