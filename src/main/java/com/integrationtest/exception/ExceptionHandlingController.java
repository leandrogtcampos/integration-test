package com.integrationtest.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.integrationtest.dto.ResponseError;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler{

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,HttpStatus status, WebRequest request) {
	    
		List<String> errors = new ArrayList<String>();
	    
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
	        errors.add(error.getField() + ": " + error.getDefaultMessage());
	    }
	    
	    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
	        errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
	    }
	    
	    ResponseError responseError =  new ResponseError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors);
	    
	    return handleExceptionInternal(ex, responseError, headers, responseError.getStatus(), request);
	}
	
	@ExceptionHandler({ IllegalArgumentException.class })
	public ResponseEntity<Object> handleMethodArgumentTypeMismatch(IllegalArgumentException ex, WebRequest request) {

	    ResponseError responseError = new ResponseError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), ex.getMessage());
	    
	    return new ResponseEntity<Object>(responseError, new HttpHeaders(), responseError.getStatus());
	}
}
