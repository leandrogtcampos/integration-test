package com.integrationtest.store;

import org.springframework.data.repository.CrudRepository;

import com.integrationtest.data.DQLRepository;

public interface StoreRepository extends CrudRepository<Store, Long>, DQLRepository<Store, Long>{
}
