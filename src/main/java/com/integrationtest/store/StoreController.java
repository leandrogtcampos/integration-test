package com.integrationtest.store;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.integrationtest.data.SpecificationUtil;
import com.integrationtest.dto.Response;

@RestController
@RequestMapping(value = "/store", produces = "application/hal+json")
public class StoreController{

	@Autowired
	StoreService service;

	@GetMapping("/{id}")
	public ResponseEntity<Response<StoreResource>> get(@PathVariable final long id) {
		return ResponseEntity.ok(new Response<StoreResource>(this.service.get(id)));
	}
		
	@GetMapping
    public ResponseEntity<PagedModel<StoreResource>> search(@Valid RequestSearchStore req){    	
    	
   		SpecificationUtil<Store> specUtil = new SpecificationUtil<Store>();
       	specUtil.and("name",req.getNome());
        	
       	return ResponseEntity.ok(this.service.search(specUtil.getSpec(), PageRequest.of(req.getPage(), req.getSize())));
    }
	
	@PostMapping(consumes = "application/json")
	public ResponseEntity<Response<StoreResource>> post(@Valid @RequestBody RequestCreateStore request) {
		StoreResource resource = this.service.save(new Store(request));
		
		return ResponseEntity.ok(new Response<StoreResource>(resource));
	}
	
	@PutMapping(consumes = "application/json")
	public ResponseEntity<Response<StoreResource>> put(@Valid @RequestBody RequestUpdateStore req) {
		StoreResource resource = this.service.get(req.getId());
		
		Store store = new Store(resource.getId(), req.getName(), req.getAddress());
		
		Response<StoreResource> response = new Response<>(this.service.update(store));
		
		return ResponseEntity.ok(response);
	}
	
	@PatchMapping(consumes = "application/json")
	public ResponseEntity<Response<StoreResource>> patch(@Valid @RequestBody RequestPatchStore req) {
		StoreResource resource = this.service.get(req.getId());
		
		Store store = new Store(resource.getId(), resource.getName(), req.getAddress());
		
		Response<StoreResource> response = new Response<>(this.service.update(store));
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Response<StoreResource>> delete(@PathVariable final long id) {
		this.service.delete(id);
		
		return ResponseEntity.ok().build();
	}
}
