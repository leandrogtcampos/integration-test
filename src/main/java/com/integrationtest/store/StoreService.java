package com.integrationtest.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StoreService {

	@Autowired
	private StoreBO bo;
	
	@Transactional(propagation=Propagation.REQUIRED, isolation=Isolation.READ_COMMITTED, readOnly=true)
	public StoreResource get(Long id) {
		return this.bo.get(id);
	}
	
	@Transactional(propagation=Propagation.REQUIRED, isolation=Isolation.READ_COMMITTED, readOnly=true)
	public PagedModel<StoreResource> search(Specification<Store> spec, Pageable pageable){
		return this.bo.search(spec, pageable);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, readOnly = false, rollbackFor = Throwable.class)
	public StoreResource save(Store store) {
		return this.bo.save(store);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, readOnly = false, rollbackFor = Throwable.class)
	public StoreResource update(Store store) {
		return this.bo.update(store);
	}
		
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED, readOnly = false, rollbackFor = Throwable.class)
	public void delete(Long id) {
		this.bo.delete(id);
	}
}
