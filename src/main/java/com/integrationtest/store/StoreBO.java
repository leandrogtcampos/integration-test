package com.integrationtest.store;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.stereotype.Component;


@Component
public class StoreBO {

	@Autowired
	private StoreRepository repository;
	
	@Autowired
	private PagedResourcesAssembler<Store> pagedResourcesAssembler;
	
	@Autowired
	private StoreResourceAssembler storeResourceAssembler;
	
	public StoreResource get(Long id) {
		return this.repository
				.findById(id)
				.map(storeResourceAssembler::toModel)
				.orElseThrow(() -> new IllegalArgumentException("Store not found."));
	}
	
	public PagedModel<StoreResource> search(Specification<Store> spec, Pageable pageable){
		Page<Store> storeList = this.repository.findAll(spec, pageable);
        return pagedResourcesAssembler.toModel(storeList, storeResourceAssembler);
	}
	
	public StoreResource save(Store store) {		
		store.setId(null);

		return storeResourceAssembler.toModel(this.repository.save(store));
	}
	
	public StoreResource update(Store store) {
		if(store.getId() == null  || !this.repository.findById(store.getId()).isPresent()) {
			throw new IllegalArgumentException("Store not found.");
		}
		
		return storeResourceAssembler.toModel(this.repository.save(store));
	}
		
	public void delete(Long id) {
		if(id == null  || !this.repository.findById(id).isPresent()) {
			throw new IllegalArgumentException("Store not found.");
		}
		
		this.repository.deleteById(id);
	}
}
