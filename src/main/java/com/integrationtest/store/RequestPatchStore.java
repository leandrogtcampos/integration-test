package com.integrationtest.store;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RequestPatchStore {

	@NotNull
	private Long id;
	
	@NotBlank
	private String address;
	
	public RequestPatchStore() {}
	
	public RequestPatchStore(Long id, String address) {
		this.id = id;
		this.address = address;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}
