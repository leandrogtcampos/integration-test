package com.integrationtest.store;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;

@Entity
@Table(name = "tb_store")
public class Store {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "store_seq")
	@SequenceGenerator(name = "store_seq", sequenceName = "store_seq")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "address")
	private String address;
	
	public Store() {}
	
	public Store(Long id, String name, String address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}
	
	public Store(String name, String address) {
		this(null, name, address);
	}
	
	public Store(RequestCreateStore request) {
		this(request.getName(), request.getAddress());
	}
	
	public Store(RequestUpdateStore request) {
		this(request.getId(), request.getName(), request.getAddress());
	}
	
	public boolean equals(Object obj) {
		if ( !(obj instanceof Store)) return false;
		
		Store other = (Store) obj;
		
		return new EqualsBuilder()
				.append(this.id, other.id)
				.isEquals();
	}
	
	public int hashCode() {
		return Objects.hashCode(this.getId());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
}