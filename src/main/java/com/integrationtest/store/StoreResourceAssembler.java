package com.integrationtest.store;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

@Component
public class StoreResourceAssembler extends RepresentationModelAssemblerSupport<Store, StoreResource> {

	public StoreResourceAssembler() {
		super(StoreController.class, StoreResource.class);
	}

	@Override
	public StoreResource toModel(Store store) {
		
		StoreResource storeResource = instantiateModel(store);
		
		storeResource.add(linkTo(StoreController.class).withRel("create").withType(HttpMethod.POST.name()));
		storeResource.add(linkTo(StoreController.class).withRel("update").withType(HttpMethod.PUT.name()));
		storeResource.add(linkTo(StoreController.class).withRel("patch").withType(HttpMethod.PATCH.name()));
		storeResource.add(linkTo(methodOn(StoreController.class).get(store.getId())).withSelfRel().withType(HttpMethod.GET.name()));
		storeResource.add(linkTo(methodOn(StoreController.class).delete(store.getId())).withRel("delete").withType(HttpMethod.DELETE.name()));
					
		storeResource.setId(store.getId()); 
		storeResource.setName(store.getName()); 
		storeResource.setAddress(store.getAddress());
					
		return storeResource;
	}
}
